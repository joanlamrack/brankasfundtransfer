package com.cloneandown.brankasfundtransfer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrankasfundtransferApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrankasfundtransferApplication.class, args);
	}
}
