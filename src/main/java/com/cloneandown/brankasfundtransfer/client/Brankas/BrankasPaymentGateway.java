package com.cloneandown.brankasfundtransfer.client.Brankas;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.cloneandown.brankasfundtransfer.client.Brankas.validatorObject.FundTransferResponse;
import com.cloneandown.brankasfundtransfer.model.WithUserAccount;
import com.google.gson.Gson;

public class BrankasPaymentGateway {
    private String apiKey;
    private Gson gson;

    public BrankasPaymentGateway() {
        gson = new Gson();
    }

    public void setupAuthentication(String apiKey) {
        this.apiKey = apiKey;
    }

    public Map<String, Object> buildPaymentGatewayRequest(WithUserAccount request) {
        String bankAccountId = request.getBankAccountId();
        String reference = request.getReference();
        String description = request.getDescription();
        String amount = request.getAmount().toString();
        String destinationCode = request.getDestinationCode();
        String destinationAccountNumber = request.getDestinationAccountNumber();
        String destinationHolder = request.getDestinationHolderName();

        Map<String, Object> destinationAmount = new HashMap<String, Object>();
        destinationAmount.put("num", amount);
        destinationAmount.put("cur", "IDR");

        Map<String, Object> address = new HashMap<String, Object>();
        address.put("line1", "Address");
        address.put("line2", "Address");
        address.put("city", "City Name");
        address.put("province", "Province name");
        address.put("zip_code", "123456");
        address.put("country", "Indonesia");

        Map<String, Object> destinationAccount = new HashMap<String, Object>();
        destinationAccount.put("bank", destinationCode);
        destinationAccount.put("number", destinationAccountNumber);
        destinationAccount.put("holder_name", destinationHolder);
        destinationAccount.put("address", address);
        destinationAccount.put("type", "PERSONAL");

        Map<String, Object> disbursementRequestMap = new HashMap<String, Object>();
        disbursementRequestMap.put("merchant_txn_id", reference);
        disbursementRequestMap.put("reference_id", reference);
        disbursementRequestMap.put("type", "PAYMENT");
        disbursementRequestMap.put("destination_account", destinationAccount);
        disbursementRequestMap.put("destination_amount", destinationAmount);

        ArrayList<Map<String, Object>> disbursementList = new ArrayList<Map<String, Object>>();
        disbursementList.add(disbursementRequestMap);

        Map<String, Object> requestMap = new HashMap<String, Object>();
        requestMap.put("source_account_id", bankAccountId);
        requestMap.put("remark", description);
        requestMap.put("disbursements", disbursementList);
        return requestMap;
    }

    public void sendTransfer(WithUserAccount request) {
        String URL = "https://disburse.sandbox.bnk.to/v2/disbursements";
        Map<String, Object> requestObject = this.buildPaymentGatewayRequest(request);

        String requestString = this.gson.toJson(requestObject);
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest requestHttpObject = HttpRequest.newBuilder()
                .uri(URI.create(URL))
                .headers(
                        "x-api-key", this.apiKey,
                        "Content-type", "application/json")
                .POST(BodyPublishers.ofString(requestString))
                .build();

        try {
            HttpResponse<String> response = client.send(
                    requestHttpObject,
                    HttpResponse.BodyHandlers.ofString());
            String rawResponse = response.body().toString();
            FundTransferResponse responseObj = this.gson.fromJson(rawResponse, FundTransferResponse.class);
            System.out.println(responseObj);
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }
    }
}
