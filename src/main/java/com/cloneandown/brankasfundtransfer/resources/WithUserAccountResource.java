package com.cloneandown.brankasfundtransfer.resources;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cloneandown.brankasfundtransfer.client.Brankas.BrankasPaymentGateway;
import com.cloneandown.brankasfundtransfer.model.WithUserAccount;
import com.cloneandown.brankasfundtransfer.repository.WithUserAccountRepository;

@RestController
public class WithUserAccountResource {
    private final WithUserAccountRepository repository;

    public WithUserAccount createFundTransfer(WithUserAccount oldFundTransfer) {
        String status = oldFundTransfer.getStatus();
        String statusDescription = oldFundTransfer.getStatusDescription();
        String reference = oldFundTransfer.getReference();
        String description = oldFundTransfer.getDescription();
        Integer amount = oldFundTransfer.getAmount();
        String destinationCode = oldFundTransfer.getDestinationCode();
        String email = oldFundTransfer.getEmail();
        String senderId = oldFundTransfer.getSenderId();
        String created = oldFundTransfer.getCreated();
        String destinationAccountNumber = oldFundTransfer.getDestinationAccountNumber();
        String bankAccountId = oldFundTransfer.getBankAccountId();

        WithUserAccount newFundTransfer = new WithUserAccount(
                status,
                statusDescription,
                reference,
                description,
                amount,
                destinationCode,
                destinationCode,
                email,
                senderId,
                created,
                destinationAccountNumber,
                bankAccountId);

        return newFundTransfer;
    }

    public WithUserAccount createFundTransfer(WithUserAccount oldFundTransfer, Integer id) {
        String status = oldFundTransfer.getStatus();
        String statusDescription = oldFundTransfer.getStatusDescription();
        String reference = oldFundTransfer.getReference();
        String description = oldFundTransfer.getDescription();
        Integer amount = oldFundTransfer.getAmount();
        String destinationCode = oldFundTransfer.getDestinationCode();
        String email = oldFundTransfer.getEmail();
        String senderId = oldFundTransfer.getSenderId();
        String created = oldFundTransfer.getCreated();
        String destinationAccountNumber = oldFundTransfer.getDestinationAccountNumber();
        String bankAccountId = oldFundTransfer.getBankAccountId();

        WithUserAccount newFundTransfer = new WithUserAccount(
                id,
                status,
                statusDescription,
                reference,
                description,
                amount,
                destinationCode,
                destinationCode,
                email,
                senderId,
                created,
                destinationAccountNumber,
                bankAccountId);

        return newFundTransfer;
    }

    public WithUserAccountResource(WithUserAccountRepository repository) {
        this.repository = repository;
    }

    @PostMapping("call/withuseraccount/save")
    public List<WithUserAccount> saveFundTransfer(@RequestBody WithUserAccount newFundTransfer) {
        WithUserAccount newFundTransferRecord = createFundTransfer(newFundTransfer);
        this.repository.save(newFundTransferRecord);
        return this.getAllFundTransfers();
    }

    @PostMapping("call/withuseraccount/update")
    public WithUserAccount updateFundTransfer(@RequestBody WithUserAccount updateFundTransfer) {
        String status = updateFundTransfer.getStatus();
        String statusDescription = updateFundTransfer.getStatusDescription();
        String reference = updateFundTransfer.getReference();
        String description = updateFundTransfer.getDescription();
        Integer amount = updateFundTransfer.getAmount();
        String destinationCode = updateFundTransfer.getDestinationCode();
        String email = updateFundTransfer.getEmail();
        String senderId = updateFundTransfer.getSenderId();
        String created = updateFundTransfer.getCreated();
        String destinationAccountNumber = updateFundTransfer.getDestinationAccountNumber();
        String bankAccountId = updateFundTransfer.getBankAccountId();

        WithUserAccount foundFundTransfer = repository.findById(updateFundTransfer.getId());
        foundFundTransfer.setStatus(status);
        foundFundTransfer.setStatusDescription(statusDescription);
        foundFundTransfer.setReference(reference);
        foundFundTransfer.setDescription(description);
        foundFundTransfer.setAmount(amount);
        foundFundTransfer.setDestinationCode(destinationCode);
        foundFundTransfer.setEmail(email);
        foundFundTransfer.setSenderId(senderId);
        foundFundTransfer.setCreated(created);
        foundFundTransfer.setDestinationAccountNumber(destinationAccountNumber);
        foundFundTransfer.setBankAccountId(bankAccountId);

        return this.repository.save(foundFundTransfer);
    }

    // TODO: check why fundtransfer also return withuseraccount fundtransfer
    @PostMapping("call/withuseraccount/list")
    public List<WithUserAccount> getAllFundTransfers() {
        return this.repository.findAll();
    }

    @PostMapping("call/withuseraccount/detail")
    public WithUserAccount getFundTransfer(@RequestParam(required = true) Integer id) {
        return this.repository.findById(id).orElse(null);
    }

    @PostMapping("call/withuseraccount/delete")
    public List<WithUserAccount> deleteFundTransfer(@RequestParam(required = true) Integer id) {
        this.repository.deleteById(id);
        return getAllFundTransfers();
    }

    @PostMapping("call/withuseraccount/sendtransfer")
    public List<WithUserAccount> sendTransfer(@RequestBody WithUserAccount newFundTransfer) {
        BrankasPaymentGateway paymentGateway = new BrankasPaymentGateway();
        paymentGateway.setupAuthentication("");

        paymentGateway.sendTransfer(newFundTransfer);

        return this.saveFundTransfer(newFundTransfer);
    }
}
