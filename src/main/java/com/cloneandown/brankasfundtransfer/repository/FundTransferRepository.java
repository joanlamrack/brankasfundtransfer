package com.cloneandown.brankasfundtransfer.repository;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cloneandown.brankasfundtransfer.model.FundTransfer;

@Repository
@Primary
public interface FundTransferRepository extends JpaRepository<FundTransfer, Integer> {
    FundTransfer findById(int id);
}