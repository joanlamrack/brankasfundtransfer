package com.cloneandown.brankasfundtransfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cloneandown.brankasfundtransfer.model.WithUserAccount;

@Repository
public interface WithUserAccountRepository extends JpaRepository<WithUserAccount, Integer> {
    WithUserAccount findById(int id);
}
