package com.cloneandown.brankasfundtransfer.client.Oy;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;

import com.cloneandown.brankasfundtransfer.client.Oy.validatorObject.FundTransferResponse;
import com.cloneandown.brankasfundtransfer.model.FundTransfer;
import com.google.gson.Gson;

public class OyPaymentGateway {
    private String apiKey;
    private String apiPassword;
    private Gson gson;

    public OyPaymentGateway() {
        gson = new Gson();
    }

    public void setupAuthentication(String apiKey, String apiPassword) {
        this.apiKey = apiKey;
        this.apiPassword = apiPassword;
    }

    public Map<String, Object> buildPaymentGatewayRequest(FundTransfer request) {
        String reference = request.getReference();
        String description = request.getDescription();
        String amount = request.getAmount().toString();
        String destinationCode = request.getDestinationCode();
        String email = request.getEmail();
        String destinationAccountNumber = request.getDestinationAccountNumber();

        Map<String, Object> requestMap = new HashMap<String, Object>();
        requestMap.put("recipient_bank", destinationCode);
        requestMap.put("recipient_account", destinationAccountNumber);
        requestMap.put("amount", amount);
        requestMap.put("note", description);
        requestMap.put("partner_trx_id", reference);
        requestMap.put("email", email);
        return requestMap;
    }

    public void sendTransfer(FundTransfer request) {
        String URL = "https://api-stg.oyindonesia.com/api/remit";
        Map<String, Object> requestObject = this.buildPaymentGatewayRequest(request);

        String requestString = this.gson.toJson(requestObject);
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest requestHttpObject = HttpRequest.newBuilder()
                .uri(URI.create(URL))
                .headers(
                        "X-OY-Username", this.apiPassword,
                        "X-Api-Key", this.apiKey,
                        "Content-type", "application/json",
                        "Accept", "application/json")
                .POST(BodyPublishers.ofString(requestString))
                .build();

        try {
            HttpResponse<String> response = client.send(
                    requestHttpObject,
                    HttpResponse.BodyHandlers.ofString());
            String rawResponse = response.body().toString();
            FundTransferResponse responseObj = this.gson.fromJson(rawResponse, FundTransferResponse.class);
            System.out.println(responseObj);
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }
    }
}
