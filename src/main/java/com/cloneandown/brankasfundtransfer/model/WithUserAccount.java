package com.cloneandown.brankasfundtransfer.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "fundTransferId")
public class WithUserAccount extends FundTransfer {
    private String bankAccountId;

    public WithUserAccount() {
        super();
    }

    public WithUserAccount(
            String status,
            String statusDescription,
            String reference,
            String description,
            Integer amount,
            String destinationCode,
            String destinationHolderName,
            String email,
            String senderId,
            String created,
            String destinationAccountNumber,
            String bankAccountId) {
        super(status, statusDescription, reference, description, amount, destinationCode, destinationHolderName, email,
                senderId, created, destinationAccountNumber);
        this.bankAccountId = bankAccountId;
    }

    public WithUserAccount(
            int id,
            String status,
            String statusDescription,
            String reference,
            String description,
            Integer amount,
            String destinationCode,
            String destinationHolderName,
            String email,
            String senderId,
            String created,
            String destinationAccountNumber,
            String bankAccountId) {
        super(
                id,
                status,
                statusDescription,
                reference,
                description,
                amount,
                destinationCode,
                destinationHolderName,
                email,
                senderId,
                created,
                destinationAccountNumber);
        this.bankAccountId = bankAccountId;
    }

    public String getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(String bankAccountId) {
        this.bankAccountId = bankAccountId;
    }
}
