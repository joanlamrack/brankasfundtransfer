package com.cloneandown.brankasfundtransfer.resources;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cloneandown.brankasfundtransfer.client.Oy.OyPaymentGateway;
import com.cloneandown.brankasfundtransfer.model.FundTransfer;
import com.cloneandown.brankasfundtransfer.repository.FundTransferRepository;

@RestController
public class FundTransferResource {
    private final FundTransferRepository repository;

    public FundTransfer createFundTransfer(FundTransfer oldFundTransfer) {
        String status = oldFundTransfer.getStatus();
        String statusDescription = oldFundTransfer.getStatusDescription();
        String reference = oldFundTransfer.getReference();
        String description = oldFundTransfer.getDescription();
        Integer amount = oldFundTransfer.getAmount();
        String destinationCode = oldFundTransfer.getDestinationCode();
        String email = oldFundTransfer.getEmail();
        String senderId = oldFundTransfer.getSenderId();
        String created = oldFundTransfer.getCreated();
        String destinationAccountNumber = oldFundTransfer.getDestinationAccountNumber();

        FundTransfer newFundTransfer = new FundTransfer(
                status,
                statusDescription,
                reference,
                description,
                amount,
                destinationCode,
                destinationCode,
                email,
                senderId,
                created,
                destinationAccountNumber);

        return newFundTransfer;
    }

    public FundTransfer createFundTransfer(FundTransfer oldFundTransfer, Integer id) {
        String status = oldFundTransfer.getStatus();
        String statusDescription = oldFundTransfer.getStatusDescription();
        String reference = oldFundTransfer.getReference();
        String description = oldFundTransfer.getDescription();
        Integer amount = oldFundTransfer.getAmount();
        String destinationCode = oldFundTransfer.getDestinationCode();
        String email = oldFundTransfer.getEmail();
        String senderId = oldFundTransfer.getSenderId();
        String created = oldFundTransfer.getCreated();
        String destinationAccountNumber = oldFundTransfer.getDestinationAccountNumber();

        FundTransfer newFundTransfer = new FundTransfer(
                id,
                status,
                statusDescription,
                reference,
                description,
                amount,
                destinationCode,
                destinationCode,
                email,
                senderId,
                created,
                destinationAccountNumber);

        return newFundTransfer;
    }

    public FundTransferResource(FundTransferRepository repository) {
        this.repository = repository;
    }

    @PostMapping("call/fundtransfer/save")
    public List<FundTransfer> saveFundTransfer(@RequestBody FundTransfer newFundTransfer) {
        FundTransfer newFundTransferRecord = createFundTransfer(newFundTransfer);
        this.repository.save(newFundTransferRecord);
        return this.getAllFundTransfers();
    }

    @PostMapping("call/fundtransfer/update")
    public FundTransfer updateFundTransfer(@RequestBody FundTransfer updateFundTransfer) {
        String status = updateFundTransfer.getStatus();
        String statusDescription = updateFundTransfer.getStatusDescription();
        String reference = updateFundTransfer.getReference();
        String description = updateFundTransfer.getDescription();
        Integer amount = updateFundTransfer.getAmount();
        String destinationCode = updateFundTransfer.getDestinationCode();
        String email = updateFundTransfer.getEmail();
        String senderId = updateFundTransfer.getSenderId();
        String created = updateFundTransfer.getCreated();
        String destinationAccountNumber = updateFundTransfer.getDestinationAccountNumber();

        FundTransfer foundFundTransfer = repository.findById(updateFundTransfer.getId());
        foundFundTransfer.setStatus(status);
        foundFundTransfer.setStatusDescription(statusDescription);
        foundFundTransfer.setReference(reference);
        foundFundTransfer.setDescription(description);
        foundFundTransfer.setAmount(amount);
        foundFundTransfer.setDestinationCode(destinationCode);
        foundFundTransfer.setEmail(email);
        foundFundTransfer.setSenderId(senderId);
        foundFundTransfer.setCreated(created);
        foundFundTransfer.setDestinationAccountNumber(destinationAccountNumber);

        return this.repository.save(foundFundTransfer);
    }

    // TODO: check why fundtransfer also return withuseraccount records
    @PostMapping("call/fundtransfer/list")
    public List<FundTransfer> getAllFundTransfers() {
        return this.repository.findAll();
    }

    @PostMapping("call/fundtransfer/detail")
    public FundTransfer getFundTransfer(@RequestParam(required = true) Integer id) {
        return this.repository.findById(id).orElse(null);
    }

    @PostMapping("call/fundtransfer/delete")
    public List<FundTransfer> deleteFundTransfer(@RequestParam(required = true) Integer id) {
        this.repository.deleteById(id);
        return getAllFundTransfers();
    }

    @PostMapping("call/fundtransfer/sendtransfer")
    public List<FundTransfer> sendTransfer(@RequestBody FundTransfer newFundTransfer) {
        OyPaymentGateway paymentGateway = new OyPaymentGateway();
        paymentGateway.setupAuthentication(
                "",
                "");

        paymentGateway.sendTransfer(newFundTransfer);

        return this.saveFundTransfer(newFundTransfer);
    }
}
